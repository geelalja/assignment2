package com.utm.csc;

/**
 * Hello world!
 *
 */
public class App 
{
  public static void main(String[] args) {
		// TODO Auto-generated method stub

			int x, y;
			String input;
			Scanner s = new Scanner(System.in);
	
			TicTacToe t = new TicTacToe();
	
			boolean hasWon = t.hasWon();
			char[][] gboard = t.getGameBoard();
			char token = 'X';
			int play = 0;
			while (hasWon == false) {
				if (play % 2 == 0) {
					token = 'X';
				} else {
					token = 'Y';
				}
				if (play > 9){
					System.out.println("Game has tied.");
					break;
				}
				System.out.println("Enter ’<row>,<col>’ to play a position. For example, ’0,2’.");
				input = s.nextLine();
				String p[] = input.split(",");
	
				x = Integer.parseInt(p[0]);
				y = Integer.parseInt(p[1]);
				if (t.checkOccupiedFirst(x, y) == false){
					--play;
				}
				t.setPosition(token, x, y);
				play++;
				hasWon = t.hasWon();
				
				char[][] formatted = t.getFormatted(gboard);
				for (int i = 0; i < formatted.length; i++) {
					for (int k = 0; k <= 4; k++) {
						System.out.print(formatted[k][i] + " ");
					}
					System.out.println();
				}
			}
			if (hasWon){
				System.out.println(t.getWinner() + " has won the game.");
			} else {
				System.out.println("The game has been tied.");
			}
	}
}
