
public class TicTacToe {
	private char[][] board;
	private int UPPER_BOUND = 2;
	private int LOWER_BOUND = 0;
	private char winner = ' ';

	public TicTacToe() {
		board = new char[3][3];
		initBoard();
	}

	public char[][] getGameBoard() {
		return board;
	}

	public void initBoard() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = '-';
			}
		}

	}

	public char[][] getFormatted(char[][] initial) {
		char[][] formatted = new char[5][5];
		formatted[1][0] = '|';
		formatted[3][0] = '|';
		formatted[0][1] = '-';
		formatted[1][1] = '+';
		formatted[2][1] = '-';
		formatted[3][1] = '+';
		formatted[4][1] = '-';
		formatted[1][2] = '|';
		formatted[3][2] = '|';
		formatted[0][3] = '-';
		formatted[1][3] = '+';
		formatted[2][3] = '-';
		formatted[3][3] = '+';
		formatted[4][3] = '-';
		formatted[1][4] = '|';
		formatted[3][4] = '|';

		formatted[0][0] = initial[0][0];
		formatted[2][0] = initial[1][0];
		formatted[0][2] = initial[0][1];
		formatted[0][4] = initial[0][2];
		formatted[2][2] = initial[1][1];
		formatted[2][4] = initial[1][2];
		formatted[4][0] = initial[2][0];
		formatted[4][2] = initial[2][1];
		formatted[4][4] = initial[2][2];

		return formatted;

	}

	public boolean checkBounds(int x, int y) {
		boolean error = false;
		if ((x < LOWER_BOUND) || (y < LOWER_BOUND)) {
			System.err.println("LOWER BOUND ERROR");
			error = true;
		}
		if ((x > UPPER_BOUND) || (y > UPPER_BOUND)) {
			System.err.println("UPPER BOUND ERROR");
			error = true;
		}
		return error;
	}

	public void setPosition(char player, int x, int y) {
		boolean offBoard = checkOnBoard(x, y);
		String coordinates = "(" + x + "," + y + ")";
		if (offBoard == false) {
			boolean inBounds = checkOnBoard(x, y);
			inBounds = !inBounds;
			boolean isOccupied = checkOccupied(x, y);
			boolean safePlay = inBounds && isOccupied;
			if (safePlay == true) {
				board[x][y] = player;
			}
		} else {
			System.err.println(
					coordinates + " is not on the board. Enter ’<row>,<col>’ to play a position. For example, ’0,2’.");
		}
	}

	public boolean checkOccupied(int x, int y) {
		boolean isOccupied = true;
		char p = board[x][y];
		if (p == '-') {
			isOccupied = true;
		} else {
			isOccupied = false;
			String coordinates = "(" + x + "," + y + ")";
			System.err.println(coordinates + " is already occupied. Please play again.");

		}
		return isOccupied;
	}

	public boolean checkOccupiedFirst(int x, int y) {
		boolean offBoard = checkOnBoard(x, y);
		boolean isOccupied = false;
		if (offBoard == false) {
			char p = board[x][y];
			if (p == '-') {
				isOccupied = true;
			} else {
				isOccupied = false;
			}
		}
		return isOccupied;
	}

	public boolean hasWon() {
		return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
	}

	public char getWinner() {
		char winChar = ' ';
		if (hasWon()) {
			winChar = winner;
		}
		return winChar;
	}

	public boolean checkRowsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[i][0], board[i][1], board[i][2]) == true) {
				winner = board[i][0];
				return true;

			}
		}
		return false;
	}

	public boolean checkOnBoard(int x, int y) {
		boolean offBoard = false;

		if (x < LOWER_BOUND) {
			offBoard = true;
		}
		if (y < LOWER_BOUND) {
			offBoard = true;
		}
		if (x > UPPER_BOUND) {
			offBoard = true;
		}
		if (y > UPPER_BOUND) {
			offBoard = true;
		}

		return offBoard;
	}

	public boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}

	public boolean checkColumnsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[0][i], board[1][i], board[2][i]) == true) {
				winner = board[0][i];
				return true;
			}
		}
		return false;
	}

	public boolean checkDiagonalsForWin() {
		boolean diagWin = ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true)
				|| (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
		winner = board[1][1];
		return diagWin;
	}

}
